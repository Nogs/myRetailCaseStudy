import restify from "restify";
import mongoose from "mongoose";
import restifyPlugins from "restify-plugins";
import routes from "./routes";

//create server
const server = restify.createServer({
  name: "myRetailStudy"
});

//implement resitfy plugins
server.use(restifyPlugins.jsonBodyParser({ mapParams: true }));
server.use(restifyPlugins.acceptParser(server.acceptable));
server.use(restifyPlugins.queryParser({ mapParams: true }));
server.use(restifyPlugins.fullResponse());

//port from process.env so heroku can set it
var port = process.env.PORT || 3000;

server.listen(port, () => {
  //if we arent running tsets, connect to MongoDB Atlas service
  if (process.env.NODE_ENV !== "test") {
    mongoose.Promise = global.Promise;
    // obviously key is temporary, 
    // and shouldn't be committed to version control
    // in a public repo in normal circumstances
    mongoose.connect(
      "mongodb+srv://Nathan:KY8mgbrYsOv9u1H1@myrpricing-a2eqo.mongodb.net/pricingAPI?retryWrites=true"
    );

    const db = mongoose.connection;

    db.on("error", err => {
      console.error(err);
      process.exit(1);
    });

    db.once("open", () => {
      console.log(`Server is listening on port 3000`);
    });
  }
  //set up routes/api
  routes(server);
});

export default server;
