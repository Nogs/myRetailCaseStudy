import errors from "restify-errors";
import axios from "axios";
import Product from "../models/product";

export default server => {
  // Get API
  server.get("/products/:id", ({ params }, res, next) => {
	// Find the product in the pricing database
    Product.findOne({ id: params.id })
      .lean()
      .exec((err, doc) => {
        if (err) {
          console.error(err);
          return next(new errors.InvalidContentError(err.errors.name.message));
		}
		//throw error if product does not exist
        if (!doc) {
          return next(
            new errors.InvalidContentError("The resource you requested could not be found.")
          );
		}
		// get product name from external api
        axios
          .get(
            `https://redsky.target.com/v2/pdp/tcin/${
              params.id
            }?excludes=taxonomy,promotion,bulk_ship,rating_and_review_reviews,rating_and_review_statistics,question_answer_statistics`
          )
          .then(({ data }) => {
			//attach name to response object
			doc.name = data.product.item.product_description.title;
			//send response
            res.send(200, doc);
            next();
		  })
		  .catch(err => {
			console.error(err);
			//throw error if product name doesn't exist in external api
			return next(
				new errors.InvalidContentError("Product name does not exist, even though pricing data does.")
			  );
		  });
    });
  });

  //put api
  server.put("/products/:id", (req, res, next) => {
	  //make sure Content-Type of applucation/json is sent
    if (!req.is("application/json")) {
      return next(new errors.InvalidContentError("Expects 'application/json'"));
    }
	// defensive check to make sure body defaults as an empty object if it doesn't exist
    let body = req.body || {};

    if (!body.id) {
      body = Object.assign({}, body, { id: req.params.id });
    }

	//Find the product and make sure it exists before we attempt to update it
    Product.findOne({ id: req.params.id }, (err, doc) => {
      if (err) {
        console.error(err);
        return next(new errors.InvalidContentError(err.errors.name.message));
	  }
	  if (!doc) {
        return next(
          new errors.ResourceNotFoundError(
            "The resource you requested could not be found."
          )
        );
      }

	  //product pricing exists, update it
      Product.update({ id: body.id }, body, err => {
        if (err) {
          console.error(err);
          return next(new errors.InvalidContentError(err.errors.name.message));
        }

        res.send(200, body);
        next();
      });
    });
  });
};
