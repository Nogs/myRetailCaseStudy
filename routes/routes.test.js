var require = require("esm")(module);
var sinon = require("sinon");
var hippie = require("hippie");

var server = require("../index").default;
var Product = require("../models/product").default;

var mockFindOne = {
  lean: function() {
    return this;
  },
  exec: function exec(callback) {
    callback(null, {
      current_price: {
        value: 13.49,
        currency_code: "USD"
      },
      id: 13860428,
      name: "The Big Lebowski (Blu-ray)"
    });
  }
};

describe("get route", function() {
  beforeEach(function() {
    sinon.stub(Product, "findOne").returns(mockFindOne);
  });

  afterEach(function() {
    Product.findOne.restore();
  });

  it("should get a product", function(done) {
    hippie(server)
      .json()
      .get("/products/13860428")
      .expectStatus(200)
      .expectBody({
        current_price: {
          value: 13.49,
          currency_code: "USD"
        },
        id: 13860428,
        name: "The Big Lebowski (Blu-ray)"
      })
      .end(function(err) {
        if (err) throw err;
        done();
      });
  });
});
