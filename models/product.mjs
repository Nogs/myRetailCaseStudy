import mongoose from "mongoose";

//model schema for pricing database
const ProductSchema = new mongoose.Schema(
  {
    id: {
      type: Number,
      required: true,
      trim: true
    },
    current_price: {
      value: {
        type: Number
      },
      currency_code: {
        type: String
      }
    }
  },
  { minimize: false }
);

export default mongoose.model("Product", ProductSchema);
