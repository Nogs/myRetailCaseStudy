# myRetail Case Study [Heroku Instance](https://myretailcasestudy.herokuapp.com/products/13860428)


Viable Product Ids:
* 13860428
* 53720577
* 53373867
* 53288696
* 53377473

Example GET: 
* URL: (http://myretailcasestudy.herokuapp.com/products/13860428)

Example PUT: 
* URL: (http://myretailcasestudy.herokuapp.com/products/13860428)
* Request Body:

```json
{
    "current_price": {
        "value": 10.99,
        "currency_code": "USD"
    }
}
```

## Running App
```js
npm install
npm start
```


## Tech/Considerations
* Runtime -[Node v10.5.0](https://nodejs.org/en/)
* Server - [Restify](http://restify.com/)
* Datastore - [MongoDB Atlas](https://www.mongodb.com/cloud/atlas)
* MongoDB ODM - [Mongoose](http://mongoosejs.com/)
* Testing Frameworks
  * [Sinon](http://sinonjs.org/)
  * [Mocha](https://mochajs.org/)
  * [Chai](http://www.chaijs.com/)
  * [Hippie](https://github.com/vesln/hippie)

Used Node 10.5.0, mostly because I wanted to experiment with Node's new ES Module implementatiom `.mjs` and also to check out the npm audit feature.

## Possible Improvements
The criteria for the case study was specifically for a Proof of Concept, as PoC's are generally to quickly show if an approach or idea is viable with shortcuts excepted, there are many ways to improve this application if it were to be used in more than just a PoC.

* Make it a complete CRUD service, instead of just a RU service.
* Implementing a proper CORS/auth/token/security system if portions of the API are not to be public.
* Possibly stream results during orchestration, so pages can begin rendering even if certain data isn't available yet.
* Dockerize the service so it can more readily scale with infrastructure improvements (clustering/load balancing/etc).
* Bring datastore away from Mongo Atlas service, use internal datastore, probably not Mongo.
* Move away from experimental `.mjs` ES Module implementation.
* More test coverage & a CI.
* Logging and logging infrastructure.